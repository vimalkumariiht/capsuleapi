﻿using DL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public class TaskOperation
    {
        public List<TaskModel> GetTask()
        {
            using (var ctx = new TaskContext())
            {
                return ctx.Tasks.ToList().Select(a => a.GetTaskModel()).ToList();
            }
        }

        public TaskModel GetTaskById(int taskId)
        {
            using (var ctx = new TaskContext())
            {
                return ctx.Tasks.FirstOrDefault(a => a.TaskId == taskId).GetTaskModel();
            }
        }

        public List<TaskModel> SearchTask(TaskModel t)
        {
            using (var ctx = new TaskContext())
            {
                var query = ctx.Tasks.Where(a =>
                    (string.IsNullOrEmpty(t.TaskDescription) || (!string.IsNullOrEmpty(t.TaskDescription) && t.TaskDescription == a.TaskDescription)) &&
                    (t.ParentId == null || (t.ParentId != null  && t.ParentId == a.ParentId)) &&
                    (t.StartDate == null || (t.StartDate != null && t.StartDate == a.StartDate)) &&
                    (t.EndDate == null || (t.EndDate != null && t.EndDate == a.EndDate)));
                var entitys = query.ToList();
                return entitys.Select(a => a.GetTaskModel()).ToList();
            }
        }

        public TaskModel AddTask(TaskModel taskModel)
        {
            using (var ctx = new TaskContext())
            {
                var entity = taskModel.GetTask();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Added;
                ctx.SaveChanges();
                return entity.GetTaskModel();
            }
        }

        public TaskModel UpdateTask(TaskModel taskModel)
        {
            using (var ctx = new TaskContext())
            {
                var entity = taskModel.GetTask();
                ctx.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                ctx.SaveChanges();
                return entity.GetTaskModel();
            }
        }
    }

    public static class Converters
    {
        public static Task GetTask(this TaskModel model)
        {
            return new Task
            {
                TaskId = model.TaskId,
                TaskDescription = model.TaskDescription,
                ParentId = model.ParentId,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Priority = model.Priority,
            };
        }

        public static TaskModel GetTaskModel(this Task model)
        {
            return new TaskModel
            {
                TaskId = model.TaskId,
                TaskDescription = model.TaskDescription,
                ParentId = model.ParentId,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                Priority = model.Priority,
            };
        }
    }
}
