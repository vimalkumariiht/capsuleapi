﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
    public class TaskContext : DbContext
    {
        public DbSet<Task> Tasks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Task>()
                .HasOptional(s => s.ParentTask)
                .WithMany(g => g.Tasks)
                .HasForeignKey(s => s.ParentId);
        }


    }
}
 
