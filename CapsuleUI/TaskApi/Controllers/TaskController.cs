﻿using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace TaskApi.Controllers
{
    [RoutePrefix("Task")]
    public class TaskController : ApiController
    {
        TaskOperation bl = new TaskOperation();
        [Route("Get/Id")]
        [HttpGet]
        public IHttpActionResult GetTaskById(int taskId)
        {
            return Ok(bl.GetTaskById(taskId));
        }

        [Route("Get")]
        [HttpGet]
        public IHttpActionResult GetTask()
        {
            return Ok(bl.GetTask());
        }

        [Route("AddTask")]
        [HttpPost]
        public IHttpActionResult AddTask(TaskModel model)
        {
            return Ok(bl.AddTask(model));
        }

        [Route("UpdateTask")]
        [HttpPost]
        public IHttpActionResult UpdateTask(TaskModel model)
        {
            return Ok(bl.UpdateTask(model));
        }

        [Route("Search")]
        [HttpPost]
        public IHttpActionResult Search(TaskModel model)
        {
            return Ok(bl.SearchTask(model));
        }
    }
}